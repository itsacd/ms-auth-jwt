package ec.edu.ista.auth;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
@Import(SpringDataRestConfiguration.class)
public class SwaggerConfig {
	ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("API Reference").version("1.0.0").build();
	}

	@Bean
	public Docket customImplementation() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).securitySchemes(Arrays.asList(apiKey()))

				.select().paths(PathSelectors.any()).apis(RequestHandlerSelectors.any()).build().pathMapping("/")
				.useDefaultResponseMessages(false).genericModelSubstitutes(ResponseEntity.class);
	}

	private ApiKey apiKey() {
		return new ApiKey("token", "token", "header");
	}

}