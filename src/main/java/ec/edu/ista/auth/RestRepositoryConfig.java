package ec.edu.ista.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import javax.persistence.metamodel.Type;

//@Configuration
public class RestRepositoryConfig extends RepositoryRestConfigurerAdapter {
    // @Value("${spring.hateoas.use-hal-as-default-json-media-type:true}")
    private boolean useHalAsDefaultJsonMediaType = true;

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.useHalAsDefaultJsonMediaType(useHalAsDefaultJsonMediaType);
    }
}