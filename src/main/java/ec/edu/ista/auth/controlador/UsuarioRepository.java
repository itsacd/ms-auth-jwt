/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ista.auth.controlador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ec.edu.ista.auth.modelado.Usuario;

// import ec.edu.ista.auth.modelado.Movilidad;

/**
 *
 * @author juan.urgilesc
 */

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query(value = "select a from Usuario a where a.usuario = :usuario")
	Usuario validarUsuario(@Param("usuario") String usuario);

	// @Query(value = "select e from Usuario e where ", countQuery = "select
	// count(e) from Evaluacion e where (e.postulacion.postulante.identificacion
	// like %:texto% or UPPER(e.postulacion.postulante.apellidoPrimero) like
	// %:texto% or UPPER(e.postulacion.postulante.nombreSegundo) like %:texto% or
	// UPPER(e.postulacion.postulante.nombrePrimero) like %:texto% or
	// UPPER(e.postulacion.postulante.apellidoSegundo) like %:texto% or
	// UPPER(e.postulacion.estado) like %:texto% or UPPER(e.postulacion.jornada)
	// like %:texto%) and e.postulacion.periodo.idPeriodo=:idPeriodo")
	// Page<Usuario> busquedaGeneral(@Param("idPeriodo") Long idPeriodo,
	// @Param("texto") String texto,
	// final Pageable pageable);
}