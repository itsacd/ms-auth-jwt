/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ista.auth.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import ec.edu.ista.auth.modelado.Modulo;
import ec.edu.ista.auth.modelado.Usuario;

// import ec.edu.ista.auth.modelado.Movilidad;

/**
 *
 * @author juan.urgilesc
 */

public interface ModuloRepository extends JpaRepository<Modulo, Integer> {
    @Query(value = "select a from Modulo a where a.nombre like %:text% or a.descripcion like %:text%")
    Page<Modulo> filtrar(@Param("text") String text, Pageable page);

}