/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ista.auth.controlador;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import ec.edu.ista.auth.modelado.Menu;

// import ec.edu.ista.auth.modelado.Movilidad;

/**
 *
 * @author juan.urgilesc
 */
public interface MenuRepository extends CrudRepository<Menu, Integer> {

}