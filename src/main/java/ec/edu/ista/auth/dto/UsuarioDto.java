package ec.edu.ista.auth.dto;

import ec.edu.ista.auth.modelado.Usuario;

public class UsuarioDto {
    private String email;
    private String usuario;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}