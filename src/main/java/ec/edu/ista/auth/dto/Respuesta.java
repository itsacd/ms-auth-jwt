package ec.edu.ista.auth.dto;

import ec.edu.ista.auth.modelado.Usuario;

public class Respuesta {
    public String token;
    public Boolean valido;
    public UsuarioDto usuario;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    public UsuarioDto getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDto usuario) {
        this.usuario = usuario;
    }

}