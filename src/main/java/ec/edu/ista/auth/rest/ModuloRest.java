package ec.edu.ista.auth.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import ec.edu.ista.auth.controlador.ModuloRepository;
import ec.edu.ista.auth.modelado.Modulo;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;

/**
 * ModuloRest
 */
@RestController
@RequestMapping("/modulo")
public class ModuloRest {
    @Autowired
    ModuloRepository moduloRepository;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                    + "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Modulo> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return moduloRepository.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                    + "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
    @CrossOrigin
    @ResponseBody
    @GetMapping("")
    public Page<Modulo> listar(@ApiIgnore final Pageable page) {
        return moduloRepository.findAll(page);
    }

    @PostMapping("")
    @CrossOrigin("*")
    public Modulo guardar(@RequestBody Modulo modulo) {
        return moduloRepository.save(modulo);
    }

    @GetMapping("/{id}")
    @CrossOrigin("*")
    public Modulo recuperar(@PathVariable Integer id) {
        return moduloRepository.findOne(id);
    }

    @DeleteMapping("/{id}")
    @CrossOrigin("*")
    public void borrar(@PathVariable Integer id) {
        moduloRepository.delete(id);
    }
}