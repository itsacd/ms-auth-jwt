package ec.edu.ista.auth.rest;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

import ec.edu.ista.auth.controlador.UsuarioRepository;
import ec.edu.ista.auth.dto.AuthDto;
import ec.edu.ista.auth.dto.Respuesta;
import ec.edu.ista.auth.dto.UsuarioDto;
import ec.edu.ista.auth.modelado.Persona;
import ec.edu.ista.auth.modelado.Usuario;
import ec.edu.ista.auth.util.JwtTokenUtil;
import ec.edu.ista.auth.util.UsuarioToken;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;

/**
 *
 * @author juanurgiles
 */
@RestController
@RequestMapping("/auth")
public class AuthRest {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenUtil jwtutil;

    /**
     * Método utilizado para guardar una entidad mediante método post
     *
     * @param entidad
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Respuesta guardar(@RequestBody AuthDto usuario) {
        try {
            // System.out.println("x+" + usuario + password);
            // System.out.println("pass" + passwordEncoder.encode(password));
            Usuario u = usuarioRepository.validarUsuario(usuario.getUsuario());
            Respuesta r = new Respuesta();
            Gson gson = new Gson();
            if (passwordEncoder.matches(usuario.getPass(), u.getPassword())) {
                r.setUsuario(gson.fromJson(gson.toJson(u), UsuarioDto.class));
                UsuarioToken ut = new UsuarioToken();
                ut.setUsername(u.getUsuario());
                ut.setIdentificacion(u.getUsuario());
                r.setValido(true);
                r.setToken(jwtutil.generateToken(ut));
            } else {
                r.setValido(false);
            }

            return r;
        } catch (Exception e) {
            e.printStackTrace();

            return null;

        }
    }

}
