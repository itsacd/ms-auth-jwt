package ec.edu.ista.auth.rest;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVReader;

import ec.edu.ista.auth.controlador.UsuarioRepository;
import ec.edu.ista.auth.dto.Respuesta;
import ec.edu.ista.auth.modelado.Persona;
import ec.edu.ista.auth.modelado.Usuario;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;

/**
 *
 * @author juanurgiles
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioRest {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Método utilizado para guardar una entidad mediante método post
     *
     * @param entidad
     * @return
     */
    @PostMapping(value = "/")
    @CrossOrigin
    public Usuario guardar(@RequestBody Usuario usuario) {
        try {
            usuario.setPassword(this.passwordEncoder.encode(usuario.getPassword()));
            return usuarioRepository.save(usuario);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void eliminar(@RequestBody Usuario usuario) {
        try {

            usuarioRepository.delete(usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo utilizado para busqueda (Mejora en paginación Ajax)
     *
     * @param pageable
     * @return
     */
    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                    + "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in
     * the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    public Page<Usuario> buscar(@ApiIgnore final Pageable pageable) {
        try {

            return usuarioRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    // @ApiOperation(value = "Find partners")
    // @ApiImplicitParams({
    // @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
    // value = "Results page you want to retrieve (0..N)"),
    // @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
    // value = "Number of records per page."),
    // @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
    // paramType = "query", value = "Sorting criteria in the format:
    // property(,asc|desc). "
    // + "Default sort order is ascending. " + "Multiple sort criteria are
    // supported.") })
    // @RequestMapping(value = "evaluacionxConvocatoria/busquedageneral/{periodo}",
    // method = RequestMethod.GET)
    // @ResponseBody
    // @CrossOrigin
    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in
     * the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     * @PathVariable Long periodo,@PathVariable String jornada,
     */
    // public Page<Usuario> busquedaGeneral(@RequestParam List<Long> texto,
    // @PathVariable Long periodo,
    // @ApiIgnore final Pageable pageable) {
    // try {

    // return usuarioRepository.busquedaGeneral(periodo, texto, pageable);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }

    // }
}
