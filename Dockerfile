FROM anapsix/alpine-java:8_server-jre_unlimited
MAINTAINER dockeruc "juan.urgilesc@gmail.com"
# set default workdir
WORKDIR /usr/Microservicio


# expose application
EXPOSE 8080
EXPOSE 8443

COPY target/*.jar /usr/Microservicio

CMD ["/bin/sh","-c","java -Dloader.path=. $JAVA_OPTS -jar *.jar"]
